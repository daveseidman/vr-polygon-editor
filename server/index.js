const express = require('express');
const bodyParser = require('body-parser');
const https = require('https');
const fs = require('fs');

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/save', (req, res1) => {
  console.log('saving');
  fs.writeFile(`${__dirname}/scenes/${new Date().toGMTString().replace(/ :+/g, '-')}.gltf`, JSON.stringify(req.body), (res2, err) => {
    res1.send({ success: err });
  });
});

const server = https.createServer({
  key: fs.readFileSync('./server/server.key'),
  cert: fs.readFileSync('./server/server.cert'),
}, app);

server.listen('8000', () => {
  console.log('server listening at https://localhost:8000');
});
