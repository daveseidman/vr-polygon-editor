// TODO: need some UI:
// Model selector (model + target)

import { ArrowHelper, Raycaster, CubeTextureLoader, WebGLRenderer, Scene, DoubleSide, PerspectiveCamera, Vector3, MeshStandardMaterial, TextureLoader, RepeatWrapping, PlaneBufferGeometry, Mesh, Color, Object3D, Fog } from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { VRButton } from 'three/examples/jsm/webxr/VRButton';
import autoBind from 'auto-bind';
import Lights from './modules/lights';
import VRConsole from './modules/vr-console';
import { XRHandModelFactory } from 'three/examples/jsm/webxr/XRHandModelFactory';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { GLTFExporter } from 'three/examples/jsm/exporters/GLTFExporter';
import { Tween, update as TweenUpdate, Easing } from '@tweenjs/tween.js';
import onChange from 'on-change';
import { radToDeg, degToRad } from './modules/utils';
import * as dat from 'dat.gui';

import './index.scss';
import px from './assets/images/background/background_1.png';
import nx from './assets/images/background/background_2.png';
import py from './assets/images/background/background_3.png';
import ny from './assets/images/background/background_4.png';
import pz from './assets/images/background/background_5.png';
import nz from './assets/images/background/background_6.png';

const grabThreshold = 0.04; // distance pinch point of hand needs to be within to grab a vertex
const pinchThreshold = 0.04; // maximum distance between thumb and forefinger tip to count as a pinch
const pushThreshold = 0.04;

class App {
  constructor() {
    autoBind(this);
    const state = {
      handsReady: false,
      pinching: [false, false],
      pushButton: [null, null],
      dragVertex: [null, null],
      dragSlider: [null, null],
      tools: [false, false],
      buttons: {
        button1: {
          percent: 0,
          active: false,
        },
        button2: {
          percent: 0,
          active: false,
        },
        button3: {
          percent: 0,
          active: false,
        },
        button4: {
          percent: 0,
          active: false,
        },
      },
    };

    this.state = onChange(state, this.update, { pathAsArray: true, ignoreKeys: ['percent', 'position'] });

    this.el = document.createElement('div');
    document.body.appendChild(this.el);

    this.saveButton = document.createElement('button');
    this.saveButton.className = 'save-button';
    this.saveButton.innerText = 'Save';
    this.saveButton.addEventListener('pointerdown', this.save);
    this.el.appendChild(this.saveButton);

    this.scene = new Scene();
    this.scene.name = 'scene';
    this.scene.background = new Color(0x878787);
    this.scene.fog = new Fog(0x878787, 10, 50);
    this.sceneObjects = new Object3D();
    this.sceneObjects.name = 'objects';
    // this.sceneObjects.position.y = 1.5;
    this.scene.add(this.sceneObjects);

    this.camera = new PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.1, 100);
    this.camera.position.set(0, 1.6, 3);

    this.gltfExporter = new GLTFExporter();

    const floorGeometry = new PlaneBufferGeometry(100, 100);
    const floorMaterial = new MeshStandardMaterial({ color: 0xeeeeee, roughness: 1.0, metalness: 0.0, map: new TextureLoader().load('assets/images/grid1.png') });
    floorMaterial.map.wrapS = RepeatWrapping;
    floorMaterial.map.wrapT = RepeatWrapping;
    floorMaterial.map.repeat.set(200, 200);

    const floor = new Mesh(floorGeometry, floorMaterial);
    floor.rotation.x = -Math.PI / 2;
    floor.receiveShadow = true;
    floor.name = 'floor';
    // this.sceneObjects.add(floor);

    this.renderer = new WebGLRenderer({ antialias: true });
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.shadowMap.enabled = true;
    this.renderer.xr.enabled = true;
    this.renderer.domElement.classList.add('scene');
    this.el.appendChild(this.renderer.domElement);

    this.controls = new OrbitControls(this.camera, this.renderer.domElement);
    this.controls.target.set(0, 1.6, 0);
    this.controls.update();

    this.headset = new Object3D();
    this.scene.add(this.headset);

    const handModelFactory = new XRHandModelFactory();

    this.hand1 = this.renderer.xr.getHand(0);
    this.hand2 = this.renderer.xr.getHand(1);
    this.hand1.add(handModelFactory.createHandModel(this.hand1, 'mesh'));
    this.hand2.add(handModelFactory.createHandModel(this.hand2, 'mesh'));

    this.handsArray = [this.hand1, this.hand2];
    this.handsObject = new Object3D();
    this.handsObject.add(this.hand1);
    this.handsObject.add(this.hand2);
    this.scene.add(this.handsObject);

    this.tools = []; // tool palettes attached to each hand

    this.lights = new Lights();
    this.sceneObjects.add(this.lights.object);

    // TODO: this should technically wait until after everything's loaded
    document.body.appendChild(VRButton.createButton(this.renderer));

    this.sceneObjects.renderer = this.renderer;
    this.sceneObjects.scene = this.scene;

    this.vrconsole = new VRConsole();
    this.sceneObjects.add(this.vrconsole.object);

    this.loadMaterials().then(() => {
      this.loadTools().then(() => {
        this.loadModels().then(() => {
          this.loadHeadset().then(() => {
            this.renderer.setAnimationLoop(this.render);

            Promise.all([this.getLeftHandMesh(), this.getRightHandMesh()]).then(() => {
              this.state.handsReady = true;

              this.hand1Raycaster = new Raycaster();
              this.hand2Raycaster = new Raycaster();
              this.hand1.getObjectByName('wrist').add(this.tools[0]);
              this.hand2.getObjectByName('wrist').add(this.tools[1]);
              this.hand1Direction = new ArrowHelper(new Vector3(0, 0, 1), new Vector3(0, 0, 0), 0.00001, 0xffff00);
              this.hand2Direction = new ArrowHelper(new Vector3(0, 0, 1), new Vector3(0, 0, 0), 0.00001, 0xffff00);
              this.hand1.getObjectByName('wrist').add(this.hand1Direction);
              this.hand2.getObjectByName('wrist').add(this.hand2Direction);

              // this.hand1.traverse((obj) => {
              //   console.log(obj.name);
              // });
              this.hand1.getObjectByName('index-finger-tip').add(this.tipPosition);
            });
          });
        });
      });
    });

    window.addEventListener('resize', this.resize, false);
  }

  loadMaterials() {
    return new Promise((resolve) => {
      const loader = new CubeTextureLoader();
      loader.load([px, nx, py, ny, pz, nz], (texture) => {
        this.scene.background = texture;

        this.modelMat = new MeshStandardMaterial({
          color: 0xbbbbbb,
          roughness: 0.2,
          metalness: 0.4,
          envMap: texture,
          envMapIntensity: 1.5,
          flatShading: true,
          side: DoubleSide,
          transparent: true,
          alphaTest: 0.1,
          // depthTest: false,
        });

        this.targetMat = new MeshStandardMaterial({
          color: 0xff3322,
          roughness: 0.4,
          transparent: true,
          opacity: 0,
          side: DoubleSide,
          flatShading: true,
          alphaTest: 0.1,
          // depthTest: false,
        });

        this.toolMat = new MeshStandardMaterial({
          color: 0xffffff,
          roughness: 0.1,
          metalness: 0.7,
          transmission: 1,
          reflectivity: 0.15,
          side: DoubleSide,
          thickness: 0.5,
          envMap: texture,
          flatShading: false,
        });

        this.handsMat = new MeshStandardMaterial({
          color: 0xeeeeee,
          roughness: 0.75,
          metalness: 0.6,
          reflectivity: 0.3,
          envMap: texture,
        });

        return resolve();
      });
    });
  }

  loadTools() {
    return new Promise((resolve) => {
      const models = {
        source: { opacity: 1, wires: false },
        target: { opacity: 0, wires: false },
      };
      this.gui = new dat.GUI();
      this.gui.add(models.source, 'opacity', 0, 1, 0.01).onChange((value) => {
        this.modelMat.opacity = value;
      });
      this.gui.add(models.source, 'wires').onChange((value) => {
        this.modelMat.wireframe = value;
      });
      this.gui.add(models.target, 'opacity', 0, 1, 0.01).onChange((value) => {
        this.targetMat.opacity = value;
      });
      this.gui.add(models.target, 'wires').onChange((value) => {
        this.targetMat.wireframe = value;
      });


      new GLTFLoader().load('assets/models/tools.gltf', (res) => {
        res.scene.traverse((obj) => {
          if (obj.isMesh && obj.material) {
            obj.material.envMap = this.scene.background;
            obj.castShadow = true;
            obj.receiveShadow = true;
          }
        });
        // console.info(res.scene.getObjectByName('Tools-Left'));
        this.tools[0] = res.scene.getObjectByName('Tools-Right');
        this.tools[0].scale.set(0, 0, 0);
        this.tools[1] = res.scene.getObjectByName('Tools-Left');
        this.toolMat.map = this.tools[1].material.map;
        // this.toolMat.alphaMap = this.tools[1].material.alphaMap;
        this.toolMat.transmissionMap = this.tools[1].material.alphaMap;

        this.tools[0].material = this.toolMat;
        this.tools[1].material = this.toolMat;

        this.tools[1].scale.set(0, 0, 0);

        this.button1 = res.scene.getObjectByName('Button1'); // this is the "container"
        this.button2 = res.scene.getObjectByName('Button2'); // this is the "container"
        this.button3 = res.scene.getObjectByName('Button3'); // this is the "container"
        this.button4 = res.scene.getObjectByName('Button4'); // this is the "container"


        this.slider1 = res.scene.getObjectByName('Slider1');
        this.handle1 = res.scene.getObjectByName('Handle1');
        this.handle1.userData.controls = 'modelMat';

        this.slider2 = res.scene.getObjectByName('Slider2');
        this.handle2 = res.scene.getObjectByName('Handle2');
        this.handle2.userData.controls = 'targetMat';

        // non-VR, put the tools somewhere we can inspect them
        if (navigator.userAgent.toLowerCase().indexOf('oculus') === -1) {
          this.sceneObjects.add(this.tools[0]);
          this.tools[0].scale.set(2, 2, 2);
          this.tools[0].position.set(0, 1.2, -0.25);
          this.tools[0].rotation.z = degToRad(-80);
          this.tools[0].rotation.x = degToRad(90);

          this.sceneObjects.add(this.tools[1]);
          this.tools[1].scale.set(2, 2, 2);
          this.tools[1].position.set(0, 1.2, 0.25);
          this.tools[1].rotation.z = degToRad(-80);
          this.tools[1].rotation.x = degToRad(90);
        }
      });
      return resolve();
    });
  }

  loadModels() {
    return new Promise((resolve) => {
      new GLTFLoader().load('assets/models/all.gltf', (res) => {
        res.scene.traverse((obj) => {
          if (obj.isMesh) obj.visible = false;
        });

        this.model = res.scene.getObjectByName('Cat');
        this.model.visible = true;
        this.model.material = this.modelMat;
        this.model.castShadow = true;
        this.model.receiveShadow = true;
        this.model.geometry.computeVertexNormals();

        this.modelTarget = res.scene.getObjectByName('Cat_target');
        this.modelTarget.visible = true;
        this.modelTarget.material = this.targetMat;
        this.modelTarget.castShadow = true;
        this.modelTarget.receiveShadow = true;
        this.modelTarget.geometry.computeVertexNormals();
        //   if (obj.isMesh) {
        //     // obj.visible = false;
        //     obj.material = this.modelMat;
        //     obj.castShadow = true;
        //     obj.receiveShadow = true;
        //   }
        // });
        // this.model.visible = true;
        this.sceneObjects.add(this.model);
        this.sceneObjects.add(this.modelTarget);
        // const testSphere = new Mesh(new SphereGeometry(1, 12, 24), this.modelMat);
        // testSphere.castShadow = true;
        // testSphere.receiveShadow = true;
        // this.sceneObjects.add(testSphere);
        return resolve();
      });
    });
  }

  loadHeadset() {
    return new Promise((resolve) => {
      new GLTFLoader().load('assets/models/headset.gltf', (res) => {
        res.scene.getObjectByName('headset').castShadow = true;
        this.raycastHeadTarget = res.scene.getObjectByName('Raycast-receive');
        this.headset.add(res.scene);
        return resolve();
      });
    });
  }

  getLeftHandMesh() {
    return new Promise((resolve) => {
      this.waitForHand1 = setInterval(() => {
        const mesh = this.hand1.getObjectByProperty('type', 'SkinnedMesh');
        if (mesh !== undefined) {
          this.hand1.mesh = mesh;
          this.hand1.mesh.castShadow = true;
          this.hand1.mesh.material = this.handsMat;
          clearInterval(this.waitForHand1);
          resolve();
        }
      }, 100);
    });
  }

  getRightHandMesh() {
    return new Promise((resolve) => {
      this.waitForHand2 = setInterval(() => {
        const mesh = this.hand2.getObjectByProperty('type', 'SkinnedMesh');
        if (mesh !== undefined) {
          this.hand2.mesh = mesh;
          this.hand2.mesh.castShadow = true;
          this.hand2.mesh.material = this.handsMat;
          clearInterval(this.waitForHand2);
          resolve();
        }
      }, 100);
    });
  }


  update(path, current, previous) {
    console.log(`${path}: ${previous} -> ${current}`);

    if (path[0] === 'pinching') {
      if (current) {
        const { point } = this.handsArray[path[1]];
        const { array } = this.model.geometry.attributes.position;
        const closest = { distance: Number.POSITIVE_INFINITY, index: -1 };
        for (let i = 0; i < array.length; i += 3) {
          const position = new Vector3(array[i + 0], array[i + 1], array[i + 2]);// .applyMatrix4(this.model.matrixWorld);
          const distance = position.distanceTo(point);
          if (distance < closest.distance) {
            closest.distance = distance;
            closest.index = i;
          }
        }
        if (closest.distance < grabThreshold) {
          this.state.dragVertex[path[1]] = closest.index;
        }

        const handle1Position = new Vector3();
        this.handle1.getWorldPosition(handle1Position);
        const distanceToHandle1 = point.distanceTo(handle1Position);
        if (distanceToHandle1 < grabThreshold) {
          this.state.dragSlider[path[1]] = this.handle1;
        }

        const handle2Position = new Vector3();
        this.handle2.getWorldPosition(handle2Position);
        const distanceToHandle2 = point.distanceTo(handle2Position);
        if (distanceToHandle2 < grabThreshold) {
          this.state.dragSlider[path[1]] = this.handle2;
        }
      } else {
        this.state.dragVertex[path[1]] = null;
        this.state.dragSlider[path[1]] = null;
      }
    }

    if (path[0] === 'tools') {
      if (current) {
        new Tween(this.tools[path[1]].scale).to({ x: 1, y: 1, z: 1 }, 500).easing(Easing.Quintic.InOut).start();
        // this.tools[path[1]].scale.set(1, 1, 1);
      } else {
        new Tween(this.tools[path[1]].scale).to({ x: 0, y: 0, z: 0 }, 500).easing(Easing.Quintic.InOut).start();

        // this.tools[path[1]].scale.set(0, 0, 0);
      }
    }

    // if (path[0] === 'button1') {
    //   if (current) this.button1Timeout = setTimeout(this.activateButton1, 2000);
    //   else if (this.button1Timeout) {
    //     clearTimeout(this.button1Timeout);
    //     this.button1Timeout = null;
    //   }
    // }
  }

  render() {
    TweenUpdate();
    this.headset.position.copy(this.camera.position);
    this.headset.quaternion.copy(this.camera.quaternion);

    const { array } = this.model.geometry.attributes.position;

    for (let i = 0; i < 2; i += 1) { // TODO: this would read better as hands.foreach
      if (this.state.dragVertex[i] !== null) {
        const { point } = this.handsArray[i];

        array[this.state.dragVertex[i] + 0] = point.x;
        array[this.state.dragVertex[i] + 1] = point.y;
        array[this.state.dragVertex[i] + 2] = point.z;
        this.model.geometry.attributes.position.needsUpdate = true; // TODO: can run after if this.state.dragVertex.some((el) => el)
      }

      if (this.state.dragSlider[i]) {
        const { point } = this.handsArray[i];
        const pointInSlider = this.state.dragSlider[i].parent.worldToLocal(point);
        const { min, max } = this.state.dragSlider[i].parent.geometry.boundingBox;
        const xTarget = Math.min(Math.max(pointInSlider.x, min.x), max.x);
        const percent = (xTarget - min.x) / (max.x - min.x);
        this.state.dragSlider[i].position.x = xTarget;// - this.state.dragSlider[i].position.x) / 2;
        this[this.state.dragSlider[i].userData.controls].opacity = 1 - percent;
      }
    }


    if (this.state.handsReady) {
      // check for pinching or pushing
      this.handsArray.forEach((hand, i) => {
        const thumbTip = hand.joints['thumb-tip'].position;
        const indexTip = hand.joints['index-finger-tip'].position;

        const pinchAmount = thumbTip.distanceTo(indexTip);

        // TODO: abstract these into an array of buttons

        const button1World = new Vector3();
        this.button1.getWorldPosition(button1World);
        const push1Amount = indexTip.distanceTo(button1World);
        if (push1Amount < pushThreshold) {
          const indexInButton = this.button1.worldToLocal(indexTip);
          const { min, max } = this.button1.geometry.boundingBox;
          const yTarget = Math.min(Math.max(indexInButton.y, min.y), max.y);
          const percent = (yTarget - min.y) / (max.y - min.y);
          if (percent === 1 && this.state.buttons.button1.percent < 1) {
            this.state.buttons.button1.active = !this.state.buttons.button1.active;
            this.button1.children[0].material.emissive.g = this.state.buttons.button1.active ? 0.5 : 0;
            this.modelMat.wireframe = this.state.buttons.button1.active;
            this.model.castShadow = !this.state.buttons.button1.active;
            this.model.receiveShadow = !this.state.buttons.button1.active;
          }
          this.state.buttons.button1.percent = (yTarget - min.y) / (max.y - min.y);
          this.button1.children[0].position.y = yTarget;// - this.state.dragSlider[i].position.x) / 2;
        }

        const button2World = new Vector3();
        this.button2.getWorldPosition(button2World);
        const push2Amount = indexTip.distanceTo(button2World);
        if (push2Amount < pushThreshold) {
          const indexInButton = this.button2.worldToLocal(indexTip);
          const { min, max } = this.button2.geometry.boundingBox;
          const yTarget = Math.min(Math.max(indexInButton.y, min.y), max.y);
          const percent = (yTarget - min.y) / (max.y - min.y);
          if (percent === 1 && this.state.buttons.button2.percent < 1) {
            this.state.buttons.button2.active = !this.state.buttons.button2.active;
            this.button2.children[0].material.emissive.g = this.state.buttons.button2.active ? 0.5 : 0;
            this.targetMat.wireframe = this.state.buttons.button2.active;
            this.modelTarget.castShadow = !this.state.buttons.button1.active;
            this.modelTarget.receiveShadow = !this.state.buttons.button1.active;
          }
          this.state.buttons.button2.percent = (yTarget - min.y) / (max.y - min.y);
          this.button2.children[0].position.y = yTarget;// - this.state.dragSlider[i].position.x) / 2;
        }

        const button3World = new Vector3();
        this.button3.getWorldPosition(button3World);
        const push3Amount = indexTip.distanceTo(button3World);
        if (push3Amount < pushThreshold) {
          const indexInButton = this.button3.worldToLocal(indexTip);
          const { min, max } = this.button3.geometry.boundingBox;
          const yTarget = Math.min(Math.max(indexInButton.y, min.y), max.y);
          const percent = (yTarget - min.y) / (max.y - min.y);
          if (percent === 1 && this.state.buttons.button3.percent < 1) {
            this.state.buttons.button3.active = !this.state.buttons.button3.active;
            this.button3.children[0].material.emissive.g = this.state.buttons.button3.active ? 0.5 : 0;
            // this.targetMat.wireframe = this.state.buttons.button3.active;
          }
          this.state.buttons.button3.percent = (yTarget - min.y) / (max.y - min.y);
          this.button3.children[0].position.y = yTarget;// - this.state.dragSlider[i].position.x) / 2;
        }

        const button4World = new Vector3();
        this.button4.getWorldPosition(button4World);
        const push4Amount = indexTip.distanceTo(button4World);
        if (push4Amount < pushThreshold) {
          const indexInButton = this.button4.worldToLocal(indexTip);
          const { min, max } = this.button4.geometry.boundingBox;
          const yTarget = Math.min(Math.max(indexInButton.y, min.y), max.y);
          const percent = (yTarget - min.y) / (max.y - min.y);
          if (percent === 1 && this.state.buttons.button4.percent < 1) {
            this.state.buttons.button4.active = !this.state.buttons.button4.active;
            this.button4.children[0].material.emissive.g = this.state.buttons.button4.active ? 0.5 : 0;
            // this.targetMat.wireframe = this.state.buttons.button4.active;
          }
          this.state.buttons.button4.percent = (yTarget - min.y) / (max.y - min.y);
          this.button4.children[0].position.y = yTarget;// - this.state.dragSlider[i].position.x) / 2;
        }


        hand.point = new Vector3((thumbTip.x + indexTip.x) / 2, (thumbTip.y + indexTip.y) / 2, (thumbTip.z + indexTip.z) / 2);
        hand.index = indexTip;
        this.state.pinching[i] = pinchAmount < pinchThreshold;
      });

      // Show / Hide tools based on when theyir raycasters are pointing at the HMD
      // TODO: reuse these vector3's or incorporate into hand object
      const worldPos1 = new Vector3();
      const worldDir1 = new Vector3();
      this.hand1Direction.getWorldPosition(worldPos1);
      this.hand1Direction.getWorldDirection(worldDir1);
      this.hand1Raycaster.set(worldPos1, worldDir1);
      const intersects1 = this.hand1Raycaster.intersectObject(this.raycastHeadTarget);
      this.state.tools[0] = intersects1.length === 1;

      const worldPos2 = new Vector3();
      const worldDir2 = new Vector3();
      this.hand2Direction.getWorldPosition(worldPos2);
      this.hand2Direction.getWorldDirection(worldDir2);
      this.hand2Raycaster.set(worldPos2, worldDir2);
      const intersects2 = this.hand2Raycaster.intersectObject(this.raycastHeadTarget);
      this.state.tools[1] = intersects2.length === 1;
    }

    this.renderer.render(this.scene, this.camera);
  }

  resize() {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
  }


  save() {
    this.gltfExporter.parse(this.model, (result) => {
      const body = JSON.stringify(result);
      fetch('/save', {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        headers: { 'Content-type': 'application/json' },
        body,
      }).then(res => res.json()).then((res) => {
        // console.log(res);
      });
    });
  }

  activateButton1() {
    console.log('activate button1');
    this.save();
  }
}

const app = new App();
if (window.location.origin.indexOf('localhost') >= 0) window.app = app;
document.body.appendChild(app.el);
