import { Object3D, HemisphereLight, DirectionalLight } from 'three';

export default class Lights {
  constructor() {
    this.object = new Object3D();
    this.object.name = 'lights';

    const hemiLight = new HemisphereLight(0xffffbb, 0x080820, 0.5);
    hemiLight.name = 'hemiLight';

    this.object.add(hemiLight);

    const dirLight = new DirectionalLight(0xdddddd, 0.4);
    dirLight.name = 'dirLight';
    dirLight.position.set(0, 6, 3);
    dirLight.castShadow = true;

    dirLight.shadow.mapSize.width = 2048;
    dirLight.shadow.mapSize.height = 2048;
    dirLight.shadow.camera.near = 0.1;
    dirLight.shadow.camera.far = 20;
    dirLight.shadow.bias = -0.001;
    dirLight.shadow.radius = 1;
    this.object.add(dirLight);
  }
}
