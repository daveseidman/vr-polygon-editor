// console window viewable in 3d space, should capture all console.logs
import { Object3D, Mesh, MeshBasicMaterial, PlaneGeometry, CanvasTexture } from 'three';

export default class VRConsole {
  constructor() {
    this.object = new Object3D();
    this.object.name = 'vrconsole';
    this.messages = [];

    const canvas = document.createElement('canvas');
    // document.body.appendChild(canvas);
    canvas.width = 400;
    canvas.height = 800;
    const context = canvas.getContext('2d');
    context.font = '12px Courier';
    context.fillStyle = '#FFF';
    context.fillRect(0, 0, context.canvas.width, context.canvas.height);

    this.console = new Mesh(new PlaneGeometry(2, 4), new MeshBasicMaterial({ color: 0xffffff, map: new CanvasTexture(canvas) }));
    this.console.scale.set(0.33, 0.33, 0.33);

    this.object.add(this.console);
    this.object.position.set(0, 1.25, 1.5);
    this.object.rotation.y = 90 * (Math.PI / 180);

    // this.object.visible = false;

    console.log = (message) => {
      console.info(message);
      this.messages.push(message);

      context.fillStyle = '#FFF';
      context.fillRect(0, 0, context.canvas.width, context.canvas.height);

      // one line at a time

      context.fillStyle = '#000';
      let count = 0;
      for (let i = this.messages.length - 1; i >= 0; i -= 1) {
        count += 1;
        context.fillText(this.messages[i], 10, count * 20);
      }
      this.console.material.map = new CanvasTexture(canvas);

      /*
      // wrapped text
      const svgCode = `
        <svg width="400" height="800" xmlns="http://www.w3.org/2000/svg">
            <foreignObject x="0" y="0" width="400" height="800">
                <style>
                p {
                    margin:0;
                    font-weight: normal;
                    font-family: serif;
                    font-size: 10px;
                    font-family: monospace:
                    line-height: 14px;
                }
                </style>
                <div xmlns="http://www.w3.org/1999/xhtml">
                    <p>${message}</p>
                </div>
            </foreignObject>
        </svg>`;

      // Remove newlines and replace double quotes with single quotes
      const svgCodeEncoded = svgCode.replace(/\n/g, '').replace(/"/g, "'");
      // console.info(svgCodeEncoded);
      // console.log(svgCodeEncoded);
      // Dynamically create an image element
      const img = document.createElement('img');
      img.onload = () => {
        console.info('loaded img', context);
        context.drawImage(img, 10, 10);
        this.console.material.map = new CanvasTexture(canvas);
      };
      img.src = `data:image/svg+xml,${svgCodeEncoded}`;
      */
    };
  }
}
