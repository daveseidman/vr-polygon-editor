import { Object3D } from 'three';
import { light } from './materials';
import autoBind from 'auto-bind';

const pinchThreshold = 0.04;

export default class Hands {
  constructor(state) {
    autoBind(this);

    this.state = state;

    this.object = new Object3D();
  }

  setScene(sceneObjects) {
    this.sceneObjects = sceneObjects;
  }

  // setHands(handsArray) {
  //   this.handsArray = handsArray;
  //   this.handsArray.forEach((hand) => {
  //     hand.mesh.material.dispose();
  //     hand.mesh.material = light;
  //     hand.mesh.material.needsUpdate = true;
  //   });
  // }

  update() {
    // if (!this.handsArray) return;

    this.handsArray.forEach((hand, index) => {
      const thumbToPointer = hand.joints['index-finger-tip'].position.distanceTo(hand.joints['thumb-tip'].position);
      hand.point = hand.joints['index-finger-tip'].position.add(hand.joints['thumb-tip'].position).divideScalar(2);
      this.state.pinching[index] = thumbToPointer < pinchThreshold;
    });
  }
}
