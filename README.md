# VR Polygon Editor  

Built for myself to make updating shapes for http://daveseidman.com easier but could be extended for other uses.

Run the server with `npm run serve` if you want to save models back to the server by calling app.save();  

Be careful to apply all scale / rotation forms to models before importing.  
